/*
 * TEA5767.h
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Yura
 */

#ifndef INC_TEA5767_H_
#define INC_TEA5767_H_



#endif /* INC_TEA5767_H_ */

#define TEA_adr_wr  	0xC0
#define TEA_adr_rd  	0xC1
#define TEA_ADDR_EEPROM    0x10 // адресс в пзу = 16 (2 сектор)
#define TEA_ADDR_CURR_CHANEL    0x16 // адресс в пзу = 22
#define TEA_ADDR_FIRST_CHANEL    0x30 // адресс в пзу первого канала. Следующие адресуюся как +16, т.е. по секторам 
#define TEA_CHANEL_MAX    20 // максимальное число каналов


#define FREQ_LO  	8800
#define FREQ_HI  	10800
#define freq_def  	10150
#define HLSI_SET  	1 //1 или 0 - выше или ниже частота синтезатора от принимаемой
#define XTAL_SET  	1 //устанавливает частоту кварца
#define PLLREF_SET 	0 //

//XTAL PLL REF Reference frequency Crystal frequency
//0			0	 		50000 Hz 			13 MHz
//0 		1 	 		50000 Hz 			6.5 MHz
//1 		0 	 		32768 Hz 			32.768 kHz
//1 		1 	 		32768 Hz 			32.768 kHz

#define HCC_SET 	0 //отвечает за установку режима обрезки высоких частот звукового сигнала.
#define SMUTE_SET 	0 //отвечает за приглушение шумов эфира
