//======================================================================================
// символы знакогенератора
// цифры
// Characters signgenerator
// Number
	//					  afedcgb.
//#define		c0		0b11111010			// 0-0
//#define		c1		0b01100000			// 1-1
//#define		c2		0b10110110			// 2-2 "z"

//					  .gfedcba
#define		c0		0b00111111			// 0-0
#define		c1		0b00000110			// 1-1
#define		c2		0b01011011			// 2-2 "z"
#define		c3		0b01001111			// 3-3
#define		c4		0b01100110			// 4-4 "Ч"
#define		c5		0b01101101			// 5-5 "S"
#define		c6		0b01111101			// 6-6
#define		c7		0b00000111			// 7-7
#define		c8		0b01111111			// 8-8
#define		c9		0b01101111			// 9-9

#define		c0t		0b10111111			// 0-0
#define		c1t		0b10000110			// 1-1
#define		c2t		0b11011011			// 2-2 "z"
#define		c3t		0b11001111			// 3-3
#define		c4t		0b11100110			// 4-4 "Ч"
#define		c5t		0b11101101			// 5-5 "S"
#define		c6t		0b11111101			// 6-6
#define		c7t		0b10000111			// 7-7
#define		c8t		0b11111111			// 8-8
#define		c9t		0b11101111			// 9-9

//======================================================================================
// символы //Characters
#define		c_		0b00000000			// символ "пробел" 
#define		cM		0b01000000			// символ "-"
#define		c__		0b00001000			// символ "подчеркивание"
#define		c_o		0b01100011			// символ "-o" верний нолик
#define		cn		0b01010100			// "n"
#define		cN		0b00110111			// "П"
#define		co		0b01011100			// "o" 
#define		cE		0b01111001			// "E"
#define		cr		0b01010000			// "r"
#define		cd		0b01011110			// "d"
#define		cb		0b01111100			// "b"
#define		ct		0b01111000			// "t"
#define		cA		0b01110111			// "A"
#define		cC		0b00111001			// "C"
#define		cH		0b01110110			// "H"
#define		cP		0b01110011			// "P"
#define		cc		0b01011000			// "c"
#define		ch		0b01110100			// "h"
#define		cL		0b00111000			// "L"
#define		ci		0b00010000			// "i"
#define		cq		0b01100111			// "q"
#define		cF		0b01110001			// "F"
#define		cU		0b00111110			// "U"
#define		cu		0b00011100			// "u"
#define		cuY		0b01101110			// "У"
#define		cI		0b00110000			// "I"
#define		cG		0b00111101			// "G"
#define		cS		0b01101101			// "S"
#define		cl		0b00011000			// "l"
#define		ca		0b01011111			// "a"
#define		cY		0b01110010			// "Y"
#define		cS		0b01101101			// "Y"
#define		ct3		0b01001001			// symbol "three features"
#define		ct2		0b01001000			// symbol "two features"
#define		ct1		0b00001000			// symbol "one feature"
//======================================================================================


