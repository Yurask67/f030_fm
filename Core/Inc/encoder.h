/*
 * encoder.h
 *
 *  Created on: 12 дек. 2021 г.
 *      Author: Yura
 */

#ifndef INC_ENCODER_H_
#define INC_ENCODER_H_



#endif /* INC_ENCODER_H_ */
#include "main.h"
/***************************************************************************************************/
/*

   - Quadrature encoder makes two waveforms that are 90° out of phase:
                           _______         _______         __
                  PinA ___|       |_______|       |_______|   PinA
          CCW <--              _______         _______
                  PinB _______|       |_______|       |______ PinB
                               _______         _______
                  PinA _______|       |_______|       |______ PinA
          CW  -->          _______         _______         __
                  PinB ___|       |_______|       |_______|   PinB
          The half of the pulses from top to bottom create full state array:
          prev.A+B   cur.A+B   (prev.AB+cur.AB)  Array   Encoder State
          -------   ---------   --------------   -----   -------------
            00         00            0000          0     stop/idle
            00         01            0001          1     CW,  0x01
            00         10            0010         -1     CCW, 0x02
            00         11            0011          0     invalid state
            01         00            0100         -1     CCW, 0x04
            01         01            0101          0     stop/idle
            01         10            0110          0     invalid state
            01         11            0111          1     CW, 0x07
            10         00            1000          1     CW, 0x08
            10         01            1001          0     invalid state
            10         10            1010          0     stop/idle
            10         11            1011         -1     CCW, 0x0B
            11         00            1100          0     invalid state
            11         01            1101         -1     CCW, 0x0D
            11         10            1110          1     CW,  0x0E
            11         11            1111          0     stop/idle
          - CW  states 0b0001, 0b0111, 0b1000, 0b1110
          - CCW states 0b0010, 0b0100, 0b1011, 0b1101

*/
/***************************************************************************************************/

/* Макросы для чтения портов энкодера и кнопки*/
#define PinA 	HAL_GPIO_ReadPin(Encod1_GPIO_Port, Encod1_Pin)
#define PinB 	HAL_GPIO_ReadPin(Encod2_GPIO_Port, Encod2_Pin)
#define Button1	HAL_GPIO_ReadPin(Button1_GPIO_Port, Button1_Pin)


/* EXTI_PR для опроса и сброса флагов прерывания*/
		//#define EXTI_PR_PinA Encod1_Pin
		//#define EXTI_PR_PinB Encod2_Pin
		//#define EXTI_PR_Button1 Button1_Pin


#define CW1 0b0001
#define CW2 0b0111
#define CW3 0b1000
#define CW4 0b1110
#define CCW1 0b0010
#define CCW2 0b0100
#define CCW3 0b1011
#define CCW4 0b1101

#define BUTTON_DEAD_TIME 100 //время нечувствительности к паразитным прерываниям от кнопки и
                             //з за дребезга. Величина в Тиках таймера (мс)

