#ifndef SW_TIMER_H_
#define SW_TIMER_H_

#define SwTimerCount  64        //Количество программных таймеров

/*Режимы работы таймеров*/
typedef enum
        {
                SWTIMER_MODE_EMPTY,
                SWTIMER_MODE_WAIT_ON,
                SWTIMER_MODE_WAIT_OFF,
                SWTIMER_MODE_CYCLE,
                SWTIMER_MODE_SINGLE
} SwTimerMode;


/*Стурктура программного таймера*/
typedef struct
    {
        unsigned LocalCount:32; //Переменная для отсчета таймера
        unsigned Count:24;              //Переменная для хранения задержки
        unsigned Mode:3;                //Режим работы
        unsigned On:1;                  //Разрешиющий бит
        unsigned Reset:1;               //Сброс счета таймера без его выключения
        unsigned Off:1;                 //Останавливающий бит
        unsigned Out:1;                 //Выход таймера
        unsigned Status:1;              //Состояние таймера
}SW_TIMER;

#if (SwTimerCount>0)
volatile SW_TIMER TIM[SwTimerCount]; //Объявление софтовых таймеров
#endif


void SwTimerWork(volatile SW_TIMER* TIM, unsigned char Count);   //Сама функция для обработки таймеров
void OnSwTimer(volatile SW_TIMER* TIM, SwTimerMode Mode, unsigned int SwCount);  //Подготовливает выбранный из массива таймер
unsigned char GetStatusSwTimer(volatile SW_TIMER* TIM);  //Считывание статуса таймера
#endif /* SW_TIMER_H_ */
//https://www.pvsm.ru/razrabotka/106255
