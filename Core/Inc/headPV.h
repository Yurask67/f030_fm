/*
 * headme.h
 *
 *  Created on: 7 нояб. 2021 г.
 *      Author: Yura
 */
//TODO данный файл включать во все другие. В этом файле инклудить и описывать глобальные переменные??
#ifndef INC_HEADME_H_
#define INC_HEADME_H_

#endif /* INC_HEADME_H_ */

// инклудим тут
#include "stdio.h"
#include "SwTimer.h"
#include "24lcxx.h"
#include "encoder.h"
#include "ml1001.h"

//глобальные переменные описываем туту

/*структура для индикатора*/
typedef struct IndicatorBufer_t
{
	uint8_t ind1_4 [4];
	uint8_t freq [5];
}IndicatorBufer_t;

/* Структура данных для записи в регистры TEA5767*/
typedef struct TEA5767WriteRegister_t
 {
	// REGISTER R1
	uint8_t PLL_13_8 :6;// PLL[13:8] setting of synthesizer programmable counter after search or preset
	uint8_t SM :1;				//поиск
	uint8_t MUTE :1;			//

	// REGISTER R2
	uint8_t PLL_7_0;// PLL[7:0] setting of synthesizer programmable counter after search or preset

	// REGISTER R3
	uint8_t SWP1 :1;		//
	uint8_t MR :1;				// MR – приглушить правый.
	uint8_t ML :1;				//ML – приглушить левый,
	uint8_t MS :1;				//MS – моно/стерео (1 или 0 соответственно)
	uint8_t HLSI :1; 			// HLSI: 1 – частота синтезатора выше принимаемой, 0 – ниже.
	uint8_t SSL :2;				//Биты SSL отвечают за остановку автопоиска. При достижении определенного
											//уровня сигнала поиск останавливается (см. даташит). Если автопоиск не нужен,
											//оставьте эти биты равными 0.
	uint8_t SUD :1;	  			// SUD – направление поиска: 1 вверх по диапазону, 0 вниз.

	// REGISTER R4
	uint8_t SI :1;				//
	uint8_t SNC :1;				//
	uint8_t HCC :1;				// HCC отвечает за установку режима обрезки высоких частот звукового сигнала.
	uint8_t SMUTE :1;			// SMUTE. Он отвечает за приглушение шумов эфира
	uint8_t XTAL :1;			// XTAL устанавливает частоту тактовки микросхемы: 1 – 32.768, 0 – 13МГц.
	uint8_t BL :1;				//
	uint8_t STBY :1;			//стандбай
	uint8_t SWP2 :1;			//

	// REGISTER R5
	uint8_t not_used :5;			 //
	uint8_t DTC :1;			 	//
	uint8_t PLLREF :1;			 //
}TEA5767WriteRegister_t;

/* Структура данных для чтения из регистров TEA5767*/
typedef struct TEA5767ReadRegister_t
{
	// REGISTER R1
		uint8_t PLL_13_8 : 6;		// PLL[13:8] setting of synthesizer programmable counter after search or preset
		uint8_t BLF : 1;			// Band Limit Flag:if BLF = 1 then the band limit has been reached; if
									// BLF = 0 then the band limit has not been reached
		uint8_t RF : 1;				// Ready Flag:if RF = 1 then a station has been found or the band limit
									// has been reached; if RF = 0 then no station has been found

		// REGISTER R2
		uint8_t PLL_7_0;			// PLL[7:0] setting of synthesizer programmable counter after search or preset

		// REGISTER R3
		uint8_t PLL_IF : 7;			// PLL[13:8] IF counter result
		uint8_t STEREO : 1;			// Stereo indication:if STEREO = 1 then stereo reception;
									// if STEREO = 0 then mono reception

		// REGISTER R4
		uint8_t Dummy : 1;			// this bit is internally set to logic 0
		uint8_t CI : 3;				// CI[3:1] Chip Identification:these bits have to be set to logic 0
		uint8_t LEV : 4;			// LEV[3:0] level ADC output

		// REGISTER R5
		uint8_t R5Data;			 	// Reserved for future extensions; these bits are internally set to logic 0
} TEA5767ReadRegister_t;

typedef struct PT2257_t
{
		uint8_t Volume; //байт управления
} PT2257_t;



/*структура для энкодера*/
typedef struct Encoder_Pin_t {
	uint8_t PinA_Old 	:1; //предыдущее значение
	uint8_t PinA_New 	:1;//новое значение
	uint8_t PinB_Old 	:1;
	uint8_t PinB_New 	:1;
	uint8_t Button_New 	:1;
	uint8_t ButtonClick :1;//флаг если был цикл нажато/отпущено в течении на менее антидребезга 
	int8_t EncCnt 		;//счетчик шагов. Положит- вправо, Отриц- влево, 0- небыло новых. Сбрасывать после обработки
	uint16_t ButtonCnt	;//счетчик времени сколько нажата кнопка до отпускания. Новое нажатие или обработка обнуляеть
	int8_t ButtonDeadTime;//время в тиках таймера нечувствительности - типа антидребезг
}Encoder_Pin_t;



/*для EEPROMa*/
typedef enum {
    OK       				= 0x00U,
    HAL__ERROR    			= 0x01U, // ошибка которую возвращает HAL_I2C_Mem_Write(..)
    DEV_OVERFLOW  			= 0x02U, // не влазит записываемый блок данных до конца EEPROM
} WRITE_StatusTypeDef;

/*имена для софтовых таймеров*/
#define TIM_ENCODER 2
#define TIM_EEPROM 3




/*объявления глобальных переменных и структур*/
extern IndicatorBufer_t volatile ind_buf; //"struct" опущен, т.к. в описании есть typedef 
extern  TEA5767WriteRegister_t volatile structwr;
extern  TEA5767ReadRegister_t volatile structrd;
extern  Encoder_Pin_t volatile struct_encoder;
extern PT2257_t volatile PT2257;//структура для РТ2257
//extern WRITE_StatusTypeDef;
extern I2C_HandleTypeDef hi2c1;

uint16_t freq; //частота
uint8_t KodMenu; //код меню
int8_t NumChannel; //номер канала
extern uint8_t NumToDig7[11];// = {c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,10}; //для преобразования цифры в 7 значный код
