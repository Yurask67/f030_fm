/*
 * PT2257.h
 *
 *  Created on: Jan 19, 2022
 *      Author: Yura
 */

#ifndef INC_PT2257_H_
#define INC_PT2257_H_
/**
 * Evc_pt2257.h - Library for using PT2257 - Electronic Volume Controller IC.
 * December 9, 2014.
 * @author Victor NPB
 * @see https://github.com/victornpb/Evc_pt2257/
 */

#ifndef Evc_pt2257_h
#define Evc_pt2257_h

#define PT2257_ADDR 0x88       //Chip address 7 bit
#define PT2257_ADDR_EEPROM    0x20 // адресс в пзу = 32 (3 сектор)
#define EVC_OFF     0b11111111  //Function OFF (-79dB)
#define EVC_2CH_1   0b11010000  //2-Channel, -1dB/step
#define EVC_2CH_10  0b11100000  //2-Channel, -10dB/step
#define EVC_L_1     0b10100000  //Left Channel, -1dB/step
#define EVC_L_10    0b10110000  //Left Channel, -10dB/step
#define EVC_R_1     0b00100000  //Right Channel, -1dB/step
#define EVC_R_10    0b00110000  //Right Channel, -10dB/step
#define EVC_MUTE    0b01111000  //2-Channel MUTE

void evc_init();

void evc_setVolume(uint8_t dB);
uint8_t evc_level(uint8_t dB);
//void evc_setVolumeLeft(uint8_t dB);
//void evc_setVolumeRight(uint8_t dB);
//void evc_mute(bool toggle);
//void evc_off();

#endif




#endif /* INC_PT2257_H_ */
