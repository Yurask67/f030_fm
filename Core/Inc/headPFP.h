/*
 * hedPFP.h
 *
 *  Created on: Dec 7, 2021
 *      Author: Yura
 */

#ifndef INC_HEADPFP_H_
#define INC_HEADPFP_H_



#endif /* INC_HEADPFP_H_ */

void indic (void);
//void SPI_SEND(uint8_t data);
unsigned char cod7_to_tic9153( unsigned char cod);
void FreqToStructInd (uint16_t chisloin);
void IndWrite(void) ;
void FreqToPllToStructwr(uint16_t f);

void TeaWrite(void);
void TeaRead(void);

void SwTimerWork(volatile SW_TIMER* TIM, unsigned char Count);
void OnSwTimer(volatile  SW_TIMER* TIM, SwTimerMode Mode, unsigned int SwCount);
unsigned char GetStatusSwTimer(volatile SW_TIMER* TIM);

void read_encoder_to_struct(void);
void read_button_to_struct(void); 

uint16_t PllToFreq(void);

WRITE_StatusTypeDef eeprom_write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress,
		uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size,
		uint32_t Timeout);
