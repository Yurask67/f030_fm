
#include "stm32f0xx_hal.h"



#define EEPROM_ADDRESS              0xA0
#define EEPROM_WRITE              	10              //time to wait in ms
#define EEPROM_TIMEOUT             	5*EEPROM_WRITE  //timeout while writing

#define	LC16 //EEPROM
#define RECORD_ARRAUND				1				// запись с нулевого адреса после заполнения всей памяти - по кругу 1- да

#ifdef LC256 										//  EEPROM 32 кб
#define EEPROM_SECTIONSIZE		  	64				// размер секции байт
#define EEPROM_MAXPKT             	32              //максимальный блок при записи байт HAL зависает при записи больших блоков
#define EEPROM_DEVSIZE              EEPROM_SECTIONSIZE*EEPROM_SECTIONCOL-1           //кол-во байт всего байт
#define EEPROM_SECTIONCOL 		  	511				//кол-во секций всего. также можно ограничить записываемые секции напр до 510
//и 511 512 использовать как не доступные для записи этой функцией
#endif

#ifdef LC16 										//  EEPROM 2 кб
#define EEPROM_SECTIONSIZE		  	16				// размер секции байт
#define EEPROM_MAXPKT             	16              //максимальный блок при записи байт HAL зависает при записи больших блоков(не больше размера сектора)
#define EEPROM_DEVSIZE              EEPROM_SECTIONSIZE*EEPROM_SECTIONCOL-1           //кол-во байт всего байт
#define EEPROM_SECTIONCOL 		  	128 			//кол-во секций всего. также можно ограничить записываемые секции напр до 510
#endif
