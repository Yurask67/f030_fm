/*
 * ml1001.c
 *
 *  Created on: Nov 5, 2021
 *      Author: Yura
 */
#include <headPV.h>
#include <headPFP.h>

#include "ml1001.h"
//#include "stdio.h"
// индикация


IndicatorBufer_t volatile ind_buf={}; // объявляем переменную IndBuf  с типом
							//структура _t и присваиваем
							//делаем ее глобальной добавив в headme.h

uint8_t NumToDig7[11] = {c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c__}; //для преобразования цифры в 7 значный код

//xxx-----------------------------------------------------------------------------------


/********засылка частоты в структуру индикатора*******/
void FreqToStructInd(uint16_t chislo) {

// Деление на 10 сдвигами и сложениями
	uint16_t n = chislo;
	uint8_t BU[5] = { 0 };
	int8_t ii = 4;
	do {
		n = n >> 1;
		n += n >> 1;
		n += n >> 4;
		n += n >> 8;
		n += n >> 16;
		uint16_t qq = n;
// делим на 8
		n >>= 3;
// вычисляем остаток
		uint8_t rem = (chislo - ((n << 1) + (qq & ~7ul))); //ul  unsigned long
// корректируем остаток и частное
		if (rem > 9) {
			rem -= 10;
			n++;
		}
		chislo = n;
		BU[ii] = rem;
		ii--;
	} while (ii >= 0);


//очистка незначащих нулей
	if (BU[0] == 0) {
		BU[0] = 10;
		if (BU[1] == 0) {
			BU[1] = 10;
			if (BU[2] == 0) {
				BU[2] = 10;
				if (BU[3] == 0) {
					BU[3] = 10;
					if (BU[4] == 0) {
						BU[4] = 10;
					}
				}
			}
		}
	}


	uint8_t i = 0;
	do {

		// switch (BU[i]) {
		// case 0:
		// 	ind_buf.freq[i] = c0;
		// 	break;
		// case 1:
		// 	ind_buf.freq[i] = c1;
		// 	break;
		// case 2:
		// 	ind_buf.freq[i] = c2;
		// 	break;
		// case 3:
		// 	ind_buf.freq[i] = c3;
		// 	break;
		// case 4:
		// 	ind_buf.freq[i] = c4;
		// 	break;
		// case 5:
		// 	ind_buf.freq[i] = c5;
		// 	break;
		// case 6:
		// 	ind_buf.freq[i] = c6;
		// 	break;
		// case 7:
		// 	ind_buf.freq[i] = c7;
		// 	break;
		// case 8:
		// 	ind_buf.freq[i] = c8;
		// 	break;
		// case 9:
		// 	ind_buf.freq[i] = c9;
		// 	break;
		// case 11:
		// 	ind_buf.freq[i] = cM;
		// 	break;
		// default:
		// 	ind_buf.freq[i] = c_;
		// 	break;
		// }
		{
			ind_buf.freq[i]= NumToDig7[BU[i]];
		}
	} while (i++ < 4);
	ind_buf.freq[2] |= 0b10000000; // добавляем точку


} //---------------------------------------------------------------------------------------


// функция преобразования 7 сегментного кода из ml1001.h в 7 сегментный код для TIC9153
unsigned char cod7_to_tic9153(unsigned char cod) {
	unsigned char cod_tmp = 0;
	cod_tmp |= ((cod >> 7) & 0b00000001);
	cod_tmp |= ((cod) & 0b00000010);
	cod_tmp |= ((cod >> 4) & 0b00000100);
	cod_tmp |= ((cod << 1) & 0b01111000);
	cod_tmp |= ((cod << 7) & 0b10000000);
	return cod_tmp;
}


