/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    stm32f0xx_it.c
 * @brief   Interrupt Service Routines.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <headPV.h>
#include <headPFP.h>
#include "ml1001.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim17;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
	while (1) {
	}
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line 0 and 1 interrupts.
  */
void EXTI0_1_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_1_IRQn 0 */

  /* USER CODE END EXTI0_1_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(Encod1_Pin);
  HAL_GPIO_EXTI_IRQHandler(Encod2_Pin);
  /* USER CODE BEGIN EXTI0_1_IRQn 1 */
	/*XXX Работает исправно именно такой вариант обработчика прерываний, с двухкратным
	сбрачыванием флагов и т.д.*/
	HAL_NVIC_DisableIRQ(EXTI0_1_IRQn);
	struct_encoder.PinA_New = PinA;
	struct_encoder.PinB_New = PinB;
	//EXTI->PR = (Encod1_Pin | Encod1_Pin);//первый сброс

	read_encoder_to_struct();

	EXTI->PR = (Encod1_Pin | Encod2_Pin);//второй сброс флагов
	HAL_NVIC_ClearPendingIRQ(EXTI0_1_IRQn); //  функция сбрасывает бит в регистре прерываний, для EXTI0_I
	HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
}
  /* USER CODE END EXTI0_1_IRQn 1 */


/**
  * @brief This function handles EXTI line 2 and 3 interrupts.
  */
void EXTI2_3_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI2_3_IRQn 0 */

  /* USER CODE END EXTI2_3_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(Button1_Pin);
  /* USER CODE BEGIN EXTI2_3_IRQn 1 */
	
	 HAL_NVIC_DisableIRQ(EXTI2_3_IRQn);
	//EXTI->PR = (Button1_Pin);//первый сброс
	struct_encoder.Button_New = Button1;
	
	read_button_to_struct();
	
	//TIM[TIM_TIME_BUTTON_PUSH].Off = 0;
	//TIM[TIM_TIME_BUTTON_PUSH].On = 1; //Запуск таймера счета времени наж. клавиши
	
	EXTI->PR = (Button1_Pin);//второй сброс
	HAL_NVIC_ClearPendingIRQ(EXTI2_3_IRQn); //  функция сбрасывает бит в регистре прерываний, для EXTI2_3
	HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

  /* USER CODE END EXTI2_3_IRQn 1 */
}

/**
  * @brief This function handles TIM17 global interrupt.
  */
void TIM17_IRQHandler(void)
{
  /* USER CODE BEGIN TIM17_IRQn 0 */

	SwTimerWork(TIM, SwTimerCount); //софт таймеры

	if ((!struct_encoder.Button_New) && (struct_encoder.ButtonCnt < 65535)) //пока нажата кнопка- увеличиваем счетчик
	{
		struct_encoder.ButtonCnt++;
	}
if (struct_encoder.ButtonDeadTime >0) struct_encoder.ButtonDeadTime--;
  /* USER CODE END TIM17_IRQn 0 */
  HAL_TIM_IRQHandler(&htim17);
  /* USER CODE BEGIN TIM17_IRQn 1 */

  /* USER CODE END TIM17_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

