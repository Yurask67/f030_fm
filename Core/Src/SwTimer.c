#include <headPV.h>
#include <headPFP.h>
#include "SwTimer.h"


void SwTimerWork(volatile SW_TIMER* TIM, unsigned char Count){
	unsigned short i=0;
		for (i=0; i<Count; i++){

			if (TIM->Mode==SWTIMER_MODE_EMPTY) {
				TIM++;
				continue;
			}

			if (TIM->Mode==SWTIMER_MODE_WAIT_ON){ //Если таймер на задержку включения
				if (TIM->On){
					if (TIM->LocalCount>0) TIM->LocalCount--;
						else {
							TIM->Out=1;
							TIM->Status=1;
						}
				}
				else {
					TIM->Out=0;
					TIM->LocalCount=TIM->Count-1;
				}
			}
			if (TIM->Mode==SWTIMER_MODE_WAIT_OFF){ //Если таймер на задержку выключения
				if (TIM->On){
					TIM->Out=1;
					TIM->Status=1;
					TIM->LocalCount=TIM->Count-1;
				}
				else {
					if (TIM->LocalCount>0) TIM->LocalCount--;
						else TIM->Out=0;
				}
			}
			if (TIM->Mode==SWTIMER_MODE_CYCLE){
				if (TIM->Off){
					if (TIM->On){
						TIM->Off=0;
						if (TIM->LocalCount>0) TIM->LocalCount--;
					}
				}
				else{
					if (TIM->LocalCount>0) {
						TIM->LocalCount--;
						TIM->Out=0;
					}
					else {
						TIM->Out=1;
						TIM->Status=1;
						TIM->LocalCount=TIM->Count-1;
					}
				}
				if (TIM->Reset){
					TIM->LocalCount=TIM->Count-1;
					TIM->Out=0;
					TIM->Status=0;
				}
			}
			if (TIM->Mode==SWTIMER_MODE_SINGLE){
				if (TIM->Off){
					if (TIM->On){
						TIM->Off=0;
						if (TIM->LocalCount>0) TIM->LocalCount--;
					}
				}
				else{
					if (TIM->LocalCount>0) {
						TIM->LocalCount--;
						TIM->Out=0;
					}
					else {
						TIM->Out=1;
						TIM->Status=1;
						TIM->LocalCount=TIM->Count-1;
						TIM->Off=1;
					}
				}
				if (TIM->Reset){
					TIM->LocalCount=TIM->Count-1;
					TIM->Out=0;
					TIM->Status=0;
				}
			}

			TIM++;
		}
}

void OnSwTimer(volatile  SW_TIMER* TIM, SwTimerMode Mode, unsigned int SwCount){
		TIM->Mode=Mode;
		if (SwCount){
			TIM->Count=SwCount;
			TIM->LocalCount=SwCount-1;
		}
		if (TIM->Mode==SWTIMER_MODE_CYCLE || TIM->Mode==SWTIMER_MODE_SINGLE){
			TIM->Off=1;
		}
}


unsigned char GetStatusSwTimer(volatile SW_TIMER* TIM){
	unsigned char status=0;
	if (TIM->Mode==SWTIMER_MODE_EMPTY) return -1;
	if (TIM->Status){
		TIM->Status=0;
		status=1;
	}
	else {
		status=0;
	}
	return status;
}

