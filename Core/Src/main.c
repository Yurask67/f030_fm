/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <headPV.h>
#include <headPFP.h>

#include "ml1001.h"
#include "TEA5767.h"
#include "encoder.h"
#include "PT2257.h"

#include <string.h> // для memcmp().
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim17;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM17_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
 {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_SPI1_Init();
	MX_I2C1_Init();
	MX_TIM17_Init();
	/* USER CODE BEGIN 2 */

	// TODO инициализация
	__disable_irq();
	HAL_TIM_Base_Start_IT(&htim17);
	//из ПЗУ
	HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, TEA_ADDR_EEPROM,
	I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, sizeof(structwr), 5000);


//	FreqToPllToStructwr(8800);
//	//инициализация каналов в ПЗУ
//	for (int var = 1; var < 20; ++var) {
//		eeprom_write(&hi2c1, EEPROM_ADDRESS,
//						TEA_ADDR_FIRST_CHANEL + 5 * (var - 1),
//						I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, sizeof(structwr),
//								5000);
//	}
//
//	//проверка записи
//	for (int var = 1; var < 20; ++var) {
//		uint8_t buff[5];
//			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS,
//				TEA_ADDR_FIRST_CHANEL + 5 * (var - 1), I2C_MEMADD_SIZE_8BIT,
//				(uint8_t*) &buff, sizeof(structwr), 5000);
//			while (memcmp(buff, (uint8_t*) &structwr, sizeof(structwr)) != 0) { //==0 значит совпадают массивы)
//		}
//	}




	TeaWrite();						//пишем
	TeaRead(); 						// тут же читаем
	FreqToStructInd(PllToFreq());	//частоту из ТЕА в структуру индикатора
	IndWrite();						//вывод на индикатор
	//чтение громкости из ПЗУ
//	eeprom_write(&hi2c1, EEPROM_ADDRESS, PT2257_ADDR_EEPROM,
//			I2C_MEMADD_SIZE_8BIT, (uint8_t*) &PT2257, sizeof(PT2257), 1000);
	HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, PT2257_ADDR_EEPROM,
			I2C_MEMADD_SIZE_8BIT, (uint8_t*) &PT2257, sizeof(PT2257), 1000);
	evc_setVolume(PT2257.Volume); //VOLUME дб

	__enable_irq(); //??

	// https://www.pvsm.ru/razrabotka/106255
	OnSwTimer(&TIM[0], SWTIMER_MODE_CYCLE, 1000); //таймер 0 - циклический 1 c
	TIM[0].Off = 0;
	TIM[0].On = 1; //Запустили циклический таймер

	OnSwTimer(&TIM[1], SWTIMER_MODE_CYCLE, 5000); //таймер 1 - циклический 5 сек
	TIM[1].Off = 0;
	TIM[1].On = 1; //Запустили циклический таймер

	OnSwTimer(&TIM[TIM_ENCODER], SWTIMER_MODE_CYCLE, 100); //таймер 1 - циклический 0.1 c  для энкодера
	TIM[TIM_ENCODER].Off = 0;
	TIM[TIM_ENCODER].On = 1;    //Запустили циклический таймер

	OnSwTimer(&TIM[TIM_EEPROM], SWTIMER_MODE_CYCLE, 15000); // - цикл 15с время ожидания перед сохр. изменений в ПЗУ
	TIM[TIM_EEPROM].Off = 1; //таймер остановлен! запускается при изменении настроек/регулировок
	TIM[TIM_EEPROM].On = 0;

// EEPROM********************************
	//	 eeprom_write(&hi2c1, EEPROM_ADDRESS , 16, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, 5, 5000);
	//	HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS , 16, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, sizeof(structwr), 5000);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	KodMenu = 0;
	while (1) {

		if (GetStatusSwTimer(&TIM[0])) { //
			if (KodMenu == 0) { //громкость
				///////////////код громкости
				ind_buf.ind1_4[0] = cU;
				ind_buf.ind1_4[1] = c0;
				ind_buf.ind1_4[2] = cL;
				ind_buf.ind1_4[3] = c__;
				FreqToStructInd(PT2257.Volume);
				ind_buf.freq[0] = cd;
				ind_buf.freq[1] = cb;
				ind_buf.freq[2] = cM;
				IndWrite(); //вывод на индикатор структуры
			}
			if (KodMenu == 1) { //читаем TEA с выводом на экран St
				TeaRead(); //читаем ТЕА в структуру
				if (structrd.STEREO) {
					ind_buf.ind1_4[0] = cS;
					ind_buf.ind1_4[1] = ct;
					ind_buf.ind1_4[2] = ind_buf.ind1_4[3] = c__;
				} else {
					ind_buf.ind1_4[0] = c__;
					ind_buf.ind1_4[1] = c__;
					ind_buf.ind1_4[2] = ind_buf.ind1_4[3] = c__;
				}
				freq = PllToFreq();    // = прочитанной из ТЕА
				FreqToStructInd(freq); // частоту в структуру индикатора
				IndWrite();                   //вывод на индикатор структуры
			}
			if (KodMenu == 2) {
				TeaRead();                    //читаем ТЕА
				freq = PllToFreq();
				FreqToStructInd(freq); // прочитанную частоту в структуру индикатора
				//далее цифра в 7-сегментный код
				ind_buf.ind1_4[0] = NumToDig7[NumChannel / 10];
				ind_buf.ind1_4[1] = NumToDig7[NumChannel % 10];
				ind_buf.ind1_4[2] = ind_buf.ind1_4[3] = c__;
				IndWrite();
			}
			if (KodMenu == 21) {
				TeaRead();                    //читаем ТЕА
				freq = PllToFreq();
				FreqToStructInd(freq); // прочитанную частоту в структуру индикатора
				ind_buf.ind1_4[0] = NumToDig7[NumChannel / 10];
				ind_buf.ind1_4[1] = NumToDig7[NumChannel % 10];
				ind_buf.ind1_4[2] = cS;
				ind_buf.ind1_4[3] = cE;
				IndWrite();
			}
		}

		///////////////ОБРАБОТКА ЕНКОДЕРА И КНОПКИ + МЕНЮ
		if (GetStatusSwTimer(&TIM[TIM_ENCODER])) {
			//**************МЕНЮ 0
			if (KodMenu == 0) //громкость
					{
				if (struct_encoder.ButtonClick
						&& (struct_encoder.ButtonCnt < 1500)) {       // 1.5 сек
					KodMenu = 1; //если было короткое нажатие кнопки -> переход меню1
					struct_encoder.ButtonClick = 0; //обнулили флаг нажатия
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
				}
				if ((struct_encoder.EncCnt /3) != 0) //если было вращение энкодера
						{
					PT2257.Volume -= (struct_encoder.EncCnt / 3); // значение енкодера / 3 + громкость
					struct_encoder.EncCnt = 0; //обнуляем счетчик енкодера после учета
					if (PT2257.Volume > 79)
						PT2257.Volume = 79;
					if (PT2257.Volume < 0)
						PT2257.Volume = 0;
					evc_setVolume(PT2257.Volume); // VOLUME дб
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
					TIM[TIM_EEPROM].Off = 0; //запуск/перезапуск таймера ПЗУ. Reset?
					TIM[TIM_EEPROM].On = 1;
					TIM[TIM_EEPROM].LocalCount = TIM[TIM_EEPROM].Count - 1; //сброс счетчика
				}
			}

			//**************МЕНЮ 1
			if (KodMenu == 1) {
				if (struct_encoder.ButtonClick
						&& (struct_encoder.ButtonCnt < 1500)) {
					KodMenu = 2; //если было короткое нажатие кнопки
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
					struct_encoder.ButtonClick = 0;
					 //читаем номер текущего канала из ПЗУ
					HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS,
					TEA_ADDR_CURR_CHANEL,
					I2C_MEMADD_SIZE_8BIT, (uint8_t*) &NumChannel,
							sizeof(NumChannel), 1000);
					HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS,
					TEA_ADDR_FIRST_CHANEL + 5 * (NumChannel - 1),
					I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr,
							sizeof(structwr), 5000);
					TeaWrite();
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
				}

				if ((struct_encoder.EncCnt /3) != 0) {
					(struct_encoder.EncCnt /3)  > 0? (structwr.HLSI = 1) : (structwr.HLSI = 0);//XXX проверить нужно ли так
          
          freq += (struct_encoder.EncCnt /3) * 10; // значение енкодера/x * 10 + частота текущая
					struct_encoder.EncCnt = 0; //обнуляем счетчик енкодера после учета
					if (freq > FREQ_HI)
						freq = FREQ_HI;
					if (freq < FREQ_LO)
						freq = FREQ_LO;
					FreqToPllToStructwr(freq);
					TeaWrite();
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
					TIM[TIM_EEPROM].Off = 0; //запуск/перезапуск таймера ПЗУ. Reset?
					TIM[TIM_EEPROM].On = 1;
					TIM[TIM_EEPROM].LocalCount = TIM[TIM_EEPROM].Count - 1; //сброс счетчика
				}
			}
			//**************МЕНЮ 2
			if (KodMenu == 2) {
				if (struct_encoder.ButtonClick
						&& struct_encoder.ButtonCnt < 1500) {
					KodMenu = 0; //если было короткое нажатие кнопки
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
					TIM[TIM_EEPROM].Status = 1;//внеочередное сохранение канала если менялся
					struct_encoder.ButtonClick = 0;
				}

				if (struct_encoder.ButtonCnt > 3000) // если длинное нажатие
								{
					KodMenu = 21;
					ind_buf.ind1_4[2] = cS;
					ind_buf.ind1_4[3] = cE;
					IndWrite();
					while (!struct_encoder.Button_New) //ожидаем отпускания кнопки
					{

					}
					struct_encoder.ButtonClick = 0;
					struct_encoder.ButtonCnt = 0;
				}

				if ((struct_encoder.EncCnt /3) != 0) //если было вращение енкодера - переключение каналов
						{
					NumChannel += (struct_encoder.EncCnt /3); // значение енкодера + номер канала
					struct_encoder.EncCnt = 0; //обнуляем счетчик енкодера после учета
					if (NumChannel > 20)
						NumChannel = 20;
					if (NumChannel < 1)
						NumChannel = 1;
					HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS,
					TEA_ADDR_FIRST_CHANEL + 5 * (NumChannel - 1),
					I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr,
							sizeof(structwr), 5000);
					TeaWrite();

					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
					TIM[TIM_EEPROM].Off = 0; //запуск/перезапуск таймера ПЗУ. Reset?
					TIM[TIM_EEPROM].On = 1;
					TIM[TIM_EEPROM].LocalCount = TIM[TIM_EEPROM].Count - 1; //сброс счетчика
				}
//				HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS,
//				TEA_ADDR_FIRST_CHANEL + 5 * (NumChannel - 1),
//				I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, sizeof(structwr),
//						5000);
			}

			if (KodMenu == 21)
			{
				if (struct_encoder.ButtonClick && struct_encoder.ButtonCnt < 1500)
				{
					KodMenu = 2; //если было короткое нажатие кнопки
					struct_encoder.ButtonClick = 0;
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
				}
				if (struct_encoder.ButtonCnt >= 3000) //запись канала в пзу
				{
					// запись канала в ПЗУ
					eeprom_write(&hi2c1, EEPROM_ADDRESS,
								 TEA_ADDR_FIRST_CHANEL + 5 * (NumChannel - 1),
								 I2C_MEMADD_SIZE_8BIT, (uint8_t *)&structwr,
								 sizeof(structwr), 5000);
					//убираем символы "SE"
					ind_buf.ind1_4[2] = ind_buf.ind1_4[3] = c__;
					IndWrite();
					while (!struct_encoder.Button_New) //ожидаем отпускания кнопки
					{
					}
					struct_encoder.ButtonCnt = 0;					
					struct_encoder.ButtonClick = 0;
					KodMenu = 2; //возврат к выбору канала
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
				}
				if ((struct_encoder.EncCnt /3) != 0)
				{
					(struct_encoder.EncCnt /3) > 0 ? (structwr.HLSI = 1) : (structwr.HLSI = 0); // XXX проверить
					//нужно ли так. Смысл в частоте синтезатора выше принимаемой, или ниже, зависит от
					//сверху или снизу подходить к частоте станции, возможно будет влиять на качество (зеркальный канал)

					freq += (struct_encoder.EncCnt /3) * 10; // значение енкодера/x * 10 + частота текущая
					struct_encoder.EncCnt = 0;			  //обнуляем счетчик енкодера после учета
					if (freq > FREQ_HI)
						freq = FREQ_HI;
					if (freq < FREQ_LO)
						freq = FREQ_LO;
					FreqToPllToStructwr(freq);
					TeaWrite();
					TIM[0].Status = 1; //внеочередная сработка софт-тймера 0????
				}
			}
						// FIXME добавить поиск станции ручной или авто

						//			HAL_SuspendTick();
						//			HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON	, PWR_STOPENTRY_WFI);
						//      if (GetStatusSwTimer(&TIM[1])) { // поиск станций через 5 сек
						//			TeaRead();
						//			FreqToPllToStructwr(PllToFreq() + 10); //увеличиваем частоту на 100 кГц
						//			//перед  следующим поиском или уменьшить при поиске вниз
						//			structwr.SM = 1;  //автопоиск старт
						//			structwr.SUD = 1; //автопоиск вверх
						//			structwr.SSL = 2; //уровень сигнала
						//			TeaWrite();
		}

		if (GetStatusSwTimer(&TIM[TIM_EEPROM])) { // проверка и запись в ПЗУ

			// FIXME здесь чтение из ПЗУ и сравнение с текущим, если совпадает, ничего не делаем + осанавливаем таймер
			//если были изменениея - пищшем новые значения
			TIM[TIM_EEPROM].Off = 1;
			TIM[TIM_EEPROM].On = 0;

			uint8_t buf[10]; //временный массив для чтения из ПЗУ и сравнения с текущим [10] - с запасом

			//----------- TEA5767
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, TEA_ADDR_EEPROM,
			I2C_MEMADD_SIZE_8BIT, (uint8_t*) &buf, sizeof(structwr), 5000);

			if (memcmp(buf, (uint8_t*) &structwr, sizeof(structwr)) != 0) { //==0 значит совпадают массивы
			//обновляем данные в ПЗУ
				eeprom_write(&hi2c1, EEPROM_ADDRESS, TEA_ADDR_EEPROM,
				I2C_MEMADD_SIZE_8BIT, (uint8_t*) &structwr, sizeof(structwr),
						5000);
			};

			//------------ теперь PT2257
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, PT2257_ADDR_EEPROM,
			I2C_MEMADD_SIZE_8BIT, (uint8_t*) &buf, sizeof(PT2257), 1000);

			if (memcmp(buf, (uint8_t*) &PT2257, sizeof(PT2257)) != 0) { //==0 значит совпадают массивы
			//обновляем данные в ПЗУ
				eeprom_write(&hi2c1, EEPROM_ADDRESS, PT2257_ADDR_EEPROM,
				I2C_MEMADD_SIZE_8BIT, (uint8_t*) &PT2257, sizeof(PT2257), 1000);
			};

			//---------------------последний прослушиваемый канал
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, TEA_ADDR_CURR_CHANEL,
			I2C_MEMADD_SIZE_8BIT, (uint8_t*) &buf, sizeof(NumChannel), 1000);
			if (memcmp(buf, (uint8_t*) &NumChannel, sizeof(NumChannel)) != 0) { //==0 значит совпадают массивы
			//обновляем данные в ПЗУ
				eeprom_write(&hi2c1, EEPROM_ADDRESS, TEA_ADDR_CURR_CHANEL,
				I2C_MEMADD_SIZE_8BIT, (uint8_t*) &NumChannel,
						sizeof(NumChannel), 1000);
			};
		}

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
}
  /* USER CODE END 3 */


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00101D2D;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_DISABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 48;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 1000;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pins : Encod1_Pin Encod2_Pin Button1_Pin */
  GPIO_InitStruct.Pin = Encod1_Pin|Encod2_Pin|Button1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_3_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

}

/* USER CODE BEGIN 4 */
/*засылка структуры в индикатор*/
void IndWrite(void) {
	int8_t ind = 0;
	do {
		uint8_t kod_ml;
		kod_ml = cod7_to_tic9153(*(ind_buf.ind1_4 + ind));

		// TODO иногда необходимо запретить глобальное прерывание для SPI I2c EEPROM и др.
		// чтобы случайно не разрешить глоб прерывание которое небыло разрешено до запрета,
		// делаем ка ниже
		uint32_t save_PM = __get_PRIMASK(); // сохраняем состояние регистра прерываний
		__disable_irq();                         // выключаем прерывания
												 // делаем что-то с глобально отключенными прерываниями
		HAL_SPI_Transmit(&hspi1, &kod_ml, 1, 1); //
		__set_PRIMASK(save_PM);                  // возвращаем "как было"
		ind++;
	} while (ind <= 8); // 9 символов
}

/******отправка структуры в ТЕА*******/
void TeaWrite(void) {

	uint8_t Status = 1;
	uint32_t save_PM = __get_PRIMASK();
	__disable_irq();
	Status = HAL_I2C_Master_Transmit(&hi2c1, TEA_adr_wr, (uint8_t*) &structwr,
			5, 1000); // 5 байт таймаут 1000 мс
	__set_PRIMASK(save_PM);
}

void TeaRead(void) {
	uint8_t Status = 1;
	uint32_t save_PM = __get_PRIMASK();
	__disable_irq();
	Status = HAL_I2C_Master_Receive(&hi2c1, TEA_adr_rd, (uint8_t*) &structrd, 5,
			1000);
	__set_PRIMASK(save_PM);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

