/*PT2257 - Electronic Volume Controller IC
Datasheet - http://www.princeton.com.tw/Portals/0/Product/PT2257.pdf
Pinout
   |-----_-----|
1 -| Lin   Rin |- 8
2 -| Lout Rout |- 7
3 -| Gnd    V+ |- 6
4 -| SDA   SCL |- 5
   |-----------|
The interface protocol consists of the following:
• A Start bit
• A Chip Address byte=88H 10001000
• ACK=Acknowledge bit
• A Data byte
• A Stop bit
Max. clock speed=100K bits/s
FUNCTION BITS
MSB    2    3    4    5    6    7    LSB  Function
----------------------------------------------------------------------------------------
  1    1    1    1    1    1    1    1    Function OFF (-79dB)
  1    1    0    1    A3   A2   A1   A0   2-Channel, -1dB/step
  1    1    1    0    0    B2   B1   B0   2-Channel, -10dB/step
  1    0    1    0    A3   A2   A1   A0   Left Channel, -1dB/step
  1    0    1    1    0    B2   B1   B0   Left Channel, -10dB/step
  0    0    1    0    A3   A2   A1   A0   Right Channel, -1dB/step
  0    0    1    1    0    B2   B1   B0   Right Channel, -10dB/step
  0    1    1    1    1    0    0    M    2-Channel MUTE (M=1 -> MUTE=ON / M=0 -> MUTE=OFF)
ATTENUATION UNIT BIT
 A3   AB2  AB1  AB0  ATT(-1) ATT(-10)
  0    0    0    0     0      0
  0    0    0    1    -1    -10
  0    0    1    0    -2    -20
  0    0    1    1    -3    -30
  0    1    0    0    -4    -40
  0    1    0    1    -5    -50
  0    1    1    0    -6    -60
  0    1    1    1    -7    -70
  1    0    0    0    -8
  1    0    0    1    -9

*/

#include <headPV.h>
#include <headPFP.h>
#include "PT2257.h"




PT2257_t volatile PT2257 = {.Volume=0};//структура для РТ2257 - 1 байт


uint8_t evc_level(uint8_t dB) {
	if (dB > 79) dB = 79;
	uint8_t b = dB / 10;  //get the most significant digit (eg. 79 gets 7)
	uint8_t a = dB % 10;  //get the least significant digit (eg. 79 gets 9)
	b = b & 0b0000111; //limit the most significant digit to 3 bit (7)
	return (b << 4) | a; //return both numbers in one byte (0BBBAAAA)
}



void evc_setVolume(uint8_t dB) {
	uint8_t bbbaaaa = evc_level(dB);
	uint8_t aaaa = bbbaaaa & 0b00001111;
	uint8_t bbb = (bbbaaaa >> 4) & 0b00001111;
	uint8_t buf[2];
	buf[0] = EVC_2CH_10 | bbb;
	buf[1] = EVC_2CH_1 | aaaa;
	uint8_t Status;

	uint32_t save_PM = __get_PRIMASK();
	__disable_irq();
	Status = HAL_I2C_Master_Transmit(&hi2c1, PT2257_ADDR, (uint8_t*) &buf, 2,
			1000); // 1 байт таймаут 1000 мс
	__set_PRIMASK(save_PM);
}

