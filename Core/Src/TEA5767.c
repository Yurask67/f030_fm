/*
 * TEA5767.c
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Yura
 */
// в этом файле будут функции работы с модулем FM

#include <headPV.h>
#include <headPFP.h>
#include "TEA5767.h"

/*структуры для работы с ТЕА*/
TEA5767WriteRegister_t volatile structwr = { .MUTE = 0, .SM = 0, .HLSI =
		HLSI_SET, .PLLREF = PLLREF_SET, .XTAL = XTAL_SET, .HCC = HCC_SET,
		.SMUTE = SMUTE_SET };
TEA5767ReadRegister_t volatile structrd;

/* функция расчета pll (формула зависит от HLSI_SET) и засылки в структуру для записи ТЕА*/
void FreqToPllToStructwr(uint16_t f)
{
	uint32_t pll;
	if (structwr.HLSI)// в зависимости от флага HLSI разные формулы
	{
		pll = (f * 10000 + 225000) >> 13;
	}
	else
	{
		pll = (f * 10000 - 225000) >> 13;
	}

	//помещаем pll в структуру
	structwr.PLL_13_8 = (pll >> 8);
	structwr.PLL_7_0 = pll & 0xff;
}

/*функция возвращает значение частоты по считанному PLL из ТЕА*/
uint16_t PllToFreq(void)
{
	uint16_t pll_read_from_tea = 0, freq_read = 0;
	pll_read_from_tea |= (uint16_t)structrd.PLL_13_8 << 8;
	pll_read_from_tea |= (uint8_t)structrd.PLL_7_0;

	if (structwr.HLSI)//FIXME будет ли совпадать с structwr? в регистрах чтения нет флага HLSI
	{
		freq_read = (((uint32_t)pll_read_from_tea << 13) - 225000) / 10000;
	}
	else
	{
		freq_read = (((uint32_t)pll_read_from_tea << 13) + 225000) / 10000;
	}

	return ++freq_read; //из-за ошибок округления результат получается freq-1
}
