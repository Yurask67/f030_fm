/*
 * encoder.c
 *
 *  Created on: 12 дек. 2021 г.
 *      Author: Yura
 */


/***************************************************************************************************/
/*

    - Quadrature encoder makes two waveforms that are 90° out of phase:
                           _______         _______         __
                  PinA ___|       |_______|       |_______|   PinA
          CCW <--              _______         _______
                  PinB _______|       |_______|       |______ PinB
                               _______         _______
                  PinA _______|       |_______|       |______ PinA
          CW  -->          _______         _______         __
                  PinB ___|       |_______|       |_______|   PinB
          The half of the pulses from top to bottom create full state array:
          prev.A+B   cur.A+B   (prev.AB+cur.AB)  Array   Encoder State
          -------   ---------   --------------   -----   -------------
            00         00            0000          0     stop/idle
            00         01            0001          1     CW,  0x01
            00         10            0010         -1     CCW, 0x02
            00         11            0011          0     invalid state
            01         00            0100         -1     CCW, 0x04
            01         01            0101          0     stop/idle
            01         10            0110          0     invalid state
            01         11            0111          1     CW, 0x07
            10         00            1000          1     CW, 0x08
            10         01            1001          0     invalid state
            10         10            1010          0     stop/idle
            10         11            1011         -1     CCW, 0x0B
            11         00            1100          0     invalid state
            11         01            1101         -1     CCW, 0x0D
            11         10            1110          1     CW,  0x0E
            11         11            1111          0     stop/idle
          - CW  states 0b0001, 0b0111, 0b1000, 0b1110
          - CCW states 0b0010, 0b0100, 0b1011, 0b1101

*/
/***************************************************************************************************/

// алгоритм с "таблицей", позволяющий увеличить точность энкодера
// в 4 раза, работает максимально чётко даже с плохими энкодерами.
// Для увеличения скорости опроса используйте PCINT и чтение из PINn
/*#define CLK 3
#define DT 2
long pos = 0;
byte lastState = 0;
const int8_t increment[16] = {0, -1, 1, 0, 1, 0, 0, -1, -1, 0, 0, 1, 0, 1, -1, 0};
void setup() {
  Serial.begin(9600);
}
void loop() {
  byte state = digitalRead(CLK) | (digitalRead(DT) << 1);
  if (state != lastState) {
    pos += increment[state | (lastState << 2)];
    lastState = state;
    Serial.println(pos);
  }
}*/

#include "encoder.h"
#include "headPV.h"
#include "headPFP.h"

Encoder_Pin_t volatile struct_encoder = {0};

/*чтение енкодера и кнопки в структуру, предварительно переносим значения полей структуры New-->Old */
inline __attribute__((always_inline)) void read_encoder_to_struct(void)
{

	// обработка полученных данных енкодер
	uint8_t KodEncoder = 0; //код в переменную
	KodEncoder |= (uint8_t) struct_encoder.PinA_Old << 3;
	KodEncoder |= (uint8_t) struct_encoder.PinB_Old << 2;
	KodEncoder |= (uint8_t) struct_encoder.PinA_New << 1;
	KodEncoder |= (uint8_t) struct_encoder.PinB_New;

	  switch (KodEncoder)
		{
	case CW1:
	case CW2:
	case CW3:
	case CW4:
		if (struct_encoder.EncCnt > -126)
			struct_encoder.EncCnt--; //от переполнения
		break;
	case CCW1:
	case CCW2:
	case CCW3:
	case CCW4:
		if (struct_encoder.EncCnt < 126)
			struct_encoder.EncCnt++; //от переполнения
		break;
	default:
		break;
	}
    
    
    /*if (KodEncoder == CW1 || KodEncoder == CW4) {
		if (struct_encoder.EncCnt < 126) {
			struct_encoder.EncCnt++;
		} //от переполнения
	}
	if (KodEncoder == CCW1 || KodEncoder == CCW4) {
		if (struct_encoder.EncCnt > -126) {
			struct_encoder.EncCnt--;
		} //от переполнения
	}*/
	struct_encoder.PinA_Old = struct_encoder.PinA_New; // новые значения теперь станут OLD
	struct_encoder.PinB_Old = struct_encoder.PinB_New;
}

/*Функция обработки кнопки*/
inline __attribute__((always_inline)) void read_button_to_struct(void)
 {
	if (!struct_encoder.Button_New) { //если кнопка только нажата т.е. отр. фронт (на входе 0)
		if (struct_encoder.ButtonDeadTime == 0) {					//не дребезг
			struct_encoder.ButtonCnt = 0;//сбросили счетчик временги удержания
			struct_encoder.ButtonClick = 0; // сбросили флаг что было нажатие/отжатие
			struct_encoder.ButtonDeadTime = BUTTON_DEAD_TIME; //задали время нечувствительности
		}
//		return; //вышли из функции
	} else { //если кнопка отпущена - полож. фронт (на входе 1)
		if (struct_encoder.ButtonDeadTime == 0) {
			struct_encoder.ButtonClick = 1; //флаг что было нажатие/отжатие
			struct_encoder.ButtonDeadTime = BUTTON_DEAD_TIME; //задали время нечувствительности
		}
	}
}
