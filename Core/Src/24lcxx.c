#include "24lcxx.h"
#include "headPV.h"
#include "headPFP.h"
//#include "main.h"
//#include "gpio_init.h"
#include "stm32f0xx_hal.h"








WRITE_StatusTypeDef eeprom_write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress,
		uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size,
		uint32_t Timeout) {

	HAL_StatusTypeDef status = OK;
	uint16_t ind = Size;

	//проверка - не влазит записываемый блок данных до конца EEPROM и не разрешена цикл запись по кругу
	if (((MemAddress + Size) > EEPROM_DEVSIZE) && RECORD_ARRAUND == 0) {

		return DEV_OVERFLOW;
	}
	//вычисляем номер страницы старта записи
	uint16_t n_page = (EEPROM_SECTIONCOL
			- (EEPROM_DEVSIZE - MemAddress) / (EEPROM_SECTIONSIZE));

	while (Size > 0) { //пока есть что писать
					   //определяемся с Size_wr****************
		uint16_t Size_wr = Size;
		if (Size >= EEPROM_MAXPKT) {
			Size_wr = EEPROM_MAXPKT;
		} //если объем записываемых данных больше
		  //максимально возможного за раз, берем максимально возможный..
		if ((MemAddress + Size_wr) >= (EEPROM_SECTIONSIZE * n_page)) { // если переходим за границу сектора,
			Size_wr = EEPROM_SECTIONSIZE * n_page - MemAddress; // ограничиваем границей сектора

			if (n_page++ > EEPROM_SECTIONCOL) {
				n_page = 1;
			} //для следующей порции записи следующая страница или первая
		}
		//END определяемся с Size_wr****************
		status = HAL_I2C_Mem_Write(hi2c, DevAddress, MemAddress, MemAddSize,
				pData + (ind - Size), Size_wr, Timeout); //пишем кусок

		Size -= Size_wr;
		//HAL_Delay(5); //FIXME зависает почему то
		int32_t zad = 0;
		while (zad < 50000) //задержка по другому, без задержки проблемы!!!
		{
			zad++;
		}
		//uint8_t pusto[2];
		//	status = HAL_I2C_Mem_Read(hi2c, DevAddress, MemAddress, MemAddSize, pusto , Size_wr, Timeout);
		if ((MemAddress += Size_wr) > EEPROM_DEVSIZE) {
			MemAddress = 0;
		} //адрес следующей порции или переход на 0 при цикл записи
	}

	return (!status ? OK : HAL__ERROR);
}

/*static uint32_t randomSeed;

 uint16_t random16(void) {
 randomSeed = randomSeed * 1103515245ul + 12345;
 return (uint16_t)((randomSeed & 0x7FFF8000) >> 15);       // take bits 30-15
 }*/
